#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>

/*
Moj program deluje tako da, ko prispes na vtipkano nadstropje (ce nisi ze prej izstopil) avtomatsko izstopis iz dvigala in se stevilka izstopnega nadstropja zapise v file trenutno_nadstropje.txt,
katero program prebere vsakic ko se hoces voziti z dvigalom (vsakic ko zazenes program). To je to.
*/

int main(){
    int nadstropje;
    int vnosNad;
    char vnosIzbire;

    FILE* file=fopen("trenutno_nadstropje.txt", "r");
    if(file==NULL){
        printf("Datoteka trenutno_nadstropje.txt ni dostopna za branje!");
        return 0;
    }

    fscanf(file, "%i", &nadstropje);        //prebere zacetno nadstropje
    fclose(file);
    //printf("%i\n",nadstropje);

    if(nadstropje<-3 || nadstropje>5){                                          //ce ugotovi da je v fajlu neki kar nebi smelo bit ga overwrita z 0
        printf("Prišlo je do zunanje manipulacije datoteke trenutno_nadstropje.txt.");
        file=fopen("trenutno_nadstropje.txt", "w");
        fprintf(file, "%i", 0);          //overwrita file z 0
        printf("Datoteka trenutno_nadstropje.txt je bila ponovno ponastavljena.");
        fclose(file);
        nadstropje = 0;
    }

    //vnos zelenega nadstropja
    do{
    printf("Ste v nadstropju %i. Vpisite stevilko zelenega nasdtropja!\n(Mozni vnosi so -3, -2, -1, 0, 1, 2, 3, 4, 5.): ", nadstropje);
    scanf(" %i",&vnosNad);
    fflush(stdin);
    }
    while(vnosNad<-3 || vnosNad>5 || vnosNad==nadstropje);
    
    if(vnosNad-nadstropje>0){nadstropje++;}else{nadstropje--;}          //dvigalo se takoj premkane v zeleno smer, ce si slucajno prestopil za samo 1 nadstropje gor ali dol, da ne vstopi v spodnjo zanko

    //zanka ki te na vsakem nadstropju vprasa ce hoces izstopit
    while(nadstropje!=vnosNad){
        do{
        printf("Ste v nadstropju stevilka %i. Ali bi radi izstopili?\n(Mozni vnosi so 0(=NE) in 1(=DA)): ", nadstropje);
        scanf(" %c",&vnosIzbire);
        fflush(stdin);
        }
        while(vnosIzbire!='0' && vnosIzbire!='1');

        if (vnosIzbire=='1'){break;}
        if(vnosNad-nadstropje>0){nadstropje++;}else{nadstropje--;}
    }

    printf("Izstopili ste iz dvigala v nadstropju %i.", nadstropje);

    //shrani izstopno tocko v file
    file=fopen("trenutno_nadstropje.txt", "w");
    fprintf(file, "%i", nadstropje);
    fclose(file);
    
    return 1;
}