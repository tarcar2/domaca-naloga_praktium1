#include <stdio.h>
#include <stdlib.h>
#include <stdbool.h>
#include <string.h>
#include <time.h>

#define MAX_STRING_LENGTH 30

void rizbica(int st);

int main(){

    srand(time(NULL));
    char beseda[MAX_STRING_LENGTH];
    char beseda_kopija[MAX_STRING_LENGTH];
    int random;

    do{
    random = rand() % 5109;     //5109 = st vrstic v datoteki + 1
    }
    while(random==0);

    FILE* file=fopen("besede.txt", "r");    //besede.txt file sem našel: https://prog.gimvic.org/krozekDokumentacija/_build/html/_downloads/besede2.txt
    if(file==NULL){
        printf("Datoteka besede.txt ni dostopna za branje!");
        return 0;
    }

    int vrstica = 1;                                            //https://www.youtube.com/watch?v=w0mgn6OLKUs
    do{                                                         //vsako vrstico iz datoteke shrani v spremenljivko beseda in se premakne 1 vrstico nizje dokler ne pride do nakljucno izbrane vrstice
        fgets(beseda, MAX_STRING_LENGTH, file);
        if(vrstica==random){break;}
        vrstica++;
    }
    while(true);
    
    fclose(file);

    bool zmaga = false;
    bool poraz = false;
    bool ugib = false;
    bool neveljavna_crka = false;
    int stevec = 0;       //stevec velikosti spremenljivke beseda
    char crka;
    char ugibane[11];     //najvec 11 ker 11-stic nepravilno ugibas in ze zgubis
    int ugibane_stevec = 0;  //stevec nepravilno ugibanih besed

    
    //printf("%s", beseda);                   //ipise bededo, ki jo moremo ugibati (pomoc pri testiranju)
    while(beseda[stevec]!='\n'){            //nastavi stevec na pravilno vrednost (stevec predstavlja dolzino besede)
        stevec++;
    }
    //printf("%i\n", stevec);
    

    for(int i=0;i<stevec;i++){beseda_kopija[i]='_';}      //beseda_kopija se napolne z '_'

    while(zmaga==false && poraz==false){
        ugib = false;
        printf("\n");

        for(int i=0;i<stevec;i++){printf("%c ", beseda_kopija[i]);}       //izpise beseda_kopija z vmesnimi presledki

        printf("\n");

        do{
            neveljavna_crka = false;
            printf("Ugibajte crko: ");
            scanf(" %c", &crka);
            //printf("%i\n",crka);         //izpise crko s stevilko (pomoc pri testiranju)
            fflush(stdin);

            for(int i=0;i<ugibane_stevec;i++){                          //preveri ce je bila vpisana ze predhodno narobe uginana crka
                if(ugibane[i]==crka){neveljavna_crka = true;break;}
            }

        }
        while(((crka<97 || crka>122) && crka!=-25 && crka!=-89 && crka!=-97) || neveljavna_crka == true);   //a-z + č, š, ž + se neugibana crka
        //OPOZORILO: Na sistemu windows 10 (kjer sem to sprogramiral) funkcionalnost č, š ž ne deluje,
        //mogoce deluje na kasnem drugem operacijskem sistemu (najverjetneje da ne deluje nikjer)

        for(int i=0;i<stevec;i++){                                  //popravi beseda_kopija in hkrati preveri ce je bil ugib pravilen
            if(beseda[i]==crka){beseda_kopija[i]=crka;ugib=true;}
        }

        if(ugib==false){                                        //napacen ugib se zapise v spremenljivko ugibane in se poveca njen stevec
            ugibane[ugibane_stevec]=crka;
            ugibane_stevec++;
        }

        printf("Ze ugibane nepravilne crke: ");                     //izpise nepravilno ugibane crke
        for(int i=0;i<ugibane_stevec;i++){
            if(i==ugibane_stevec-1){printf("%c.\n", ugibane[i]);}
            else{printf("%c, ", ugibane[i]);}
        }
        
        printf("\n");
        rizbica(ugibane_stevec);   //narise slikico
        
        if(ugib==true){                         //preveri ce je uporabnik zmagal ali izgubil
            zmaga=true;
            for(int i=0;i<stevec;i++){
                if(beseda[i]!=beseda_kopija[i]){zmaga=false;break;}
            } 
        }
        else{if(ugibane_stevec==11){poraz=true;}}
    }
    
    printf("\n");                                   //izpis zakljucnega sporocila
    if(ugib==true){printf("Celo ti je ratalo ej. Ta je bila enostavna sigurno. Vec srece ko pameti.");}
    else{printf("Ko si slab ej. Ocitno si nepismen. Skrita beseda je bila %s", beseda);}

    return 1;
}


void rizbica(int st){
    switch (st){
        case 0:break;
        case 1:
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 2:
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 3:
        printf("     _____________\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 4:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 5:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        (     )\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 6:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        ( ^_^ )\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("     |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 7:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        ( ^_^ )\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 8:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        ( ^_^ )\n");
        printf("     |           |\n");
        printf("     |          /|\n");
        printf("     |         / |\n");
        printf("     |           |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 9:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        ( ^_^ )\n");
        printf("     |           |\n");
        printf("     |          /|\\\n");
        printf("     |         / | \\\n");
        printf("     |           |\n");
        printf("  ___|___\n");
        printf(" /       \\\n");
        printf("/         \\\n");
        break;
        case 10:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        ( ^_^ )\n");
        printf("     |           |\n");
        printf("     |          /|\\\n");
        printf("     |         / | \\\n");
        printf("     |           |\n");
        printf("  ___|___       /\n");
        printf(" /       \\     /\n");
        printf("/         \\\n");
        break;
        case 11:
        printf("     _____________\n");
        printf("     |           |\n");
        printf("     |           |\n");
        printf("     |        ( ^_^ )\n");
        printf("     |           |\n");
        printf("     |          /|\\\n");
        printf("     |         / | \\\n");
        printf("     |           |\n");
        printf("  ___|___       / \\\n");
        printf(" /       \\     /   \\\n");
        printf("/         \\\n");
        break;
        default:
        printf("Nevem kako ti je to ratalo, stvar je shekana, potrjeno.");
        break;
    }

}